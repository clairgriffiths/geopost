class PostsController < ApplicationController
  
  before_action :find_post, except: [:index, :new, :create]

# runs the find_post method before all of these so that you always have the post you are refering to
# before_action - a class method that is built in
# need to say which of the methods you want it to be run before
# :except - is the key to a hash

  def index
    if params[:featured].present? and params[:featured] == "true"
      #get only the featured posts 
      @posts = Post.where(is_featured: true).order("created_at DESC")
    else
  	  @posts = Post.order("created_at DESC")
    end
  end
  # true is in quotes as it is being returned as a string, not a boolean value
  # 


    def show
      @comments = @post.comments
  #	@post = Post.find(params[:id])
  end
 # each of these @posts are only available within their methods (which is why you are using the @ symbol!!)

  def new
    @post = Post.new

  end

# The new form gets submitted to the create action
  
  def create
    @post = Post.new(post_params)
      if @post.save
        flash[:success] = "Success!"
      redirect_to root_path
    else
      flash[:error] = "Oops, something went wrong, please try again"
      render :new
    end
  end


#flash is a hash, [:error] is reading the key

 ###################RELATED########

  def edit
    #@post = Post.find(params[:id])
  end

# The edit action gets submitted to the update action
  
  def update
    #@post = Post.find(params[:id])
      if @post.update(post_params)
        flash[:success] = "Success! Updated '#{@post.title}'"
        redirect_to post_path(@post)
      else
        flash[:error] = "Oops, something went wrong, please try again"
        render :edit
      end
  end

  #render means that nothing saved. Had we redirected instead, it would have saved it

####################RELATED#########

  def destroy
    #@post = Post.find(params[:id])
    @post.destroy
    redirect_to root_path
  end


# allow attributes that I trust, ignore any scary malicious unsanctioned evil data
private
def post_params
  params.require(:post).permit(:title, :body, :is_required, :is_published, :is_featured)
  end

end

#strong parameters - changed recently

#POST requests are made when you click submit, the data ends up in the params hash. You have to whitelist 
#ANY requests end up in the params hash!

def find_post
 @post = Post.find(params[:id])
end