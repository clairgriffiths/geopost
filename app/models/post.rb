class Post < ActiveRecord::Base

	has_many :comments

validates :title, presence: true, uniqueness: true
validates :body, length: {minimum: 1 }

#validates takes arguments- Name of the column, thing you are validation: what it needs to pass
# presence: true - is just a hash!
#length is a key to a hash and the value is another hash with the hey of minimum

end

