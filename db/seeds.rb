# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Post.all.delete_all

Post.create([
  		{"title" => "Hello", "body" => "World!"},
  		{"title" => "Test 1", "body" => "Test 2, here is a new line eeeeeeeeee eeeeeeeeeeee eeeeeeeeeeeeee eeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeeeeeeeee eeeeeeee"},
  		{"title" => "Third 'seed' test", "body" => "Test 3"}
  		])

