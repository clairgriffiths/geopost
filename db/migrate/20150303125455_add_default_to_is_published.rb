class AddDefaultToIsPublished < ActiveRecord::Migration
  def change

  	change_column :posts, :is_published, :boolean, default: false

  end
end
